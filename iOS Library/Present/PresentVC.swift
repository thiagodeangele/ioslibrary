//
//  PresentViewController.swift
//  Swift Library
//
//  Created by Thiago de Angele on 18/08/21.
//

import UIKit

class PresentVC: UIViewController {
    
    let navButton = BotaoVoltar()

    let botaoModal: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("ABRIR MODAL", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(abrirModal), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let botaoFullscreen: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("ABRIR FULLSCREEN", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(abrirFull), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        addSubview()
        
        setupConstraints()
        
        configConstraints()//Antigo
        
        navButton.btnVoltar.addTarget(self, action: #selector(botaoFechar), for: .touchUpInside)

 
    }
    
    // MARK: - LINKS
    
    @objc func abrirModal(){
        
        let controllerDestino = DismissViewController()
        let navigationViewController = UINavigationController(rootViewController: controllerDestino)
        
        controllerDestino.title = "STYLE MODAL"
        controllerDestino.cor = .systemPink
        controllerDestino.textoInformativo.text = "ABRIU NO MODO\nMODAL"
        navigationViewController.modalPresentationStyle = .automatic
        
        present(navigationViewController, animated: true, completion: nil)
    }
    
    
    @objc func abrirFull(){
        
        let controllerDestino = DismissViewController()
        let navigationViewController = UINavigationController(rootViewController: controllerDestino)

        controllerDestino.title = "STYLE FULLSCREEN"
        controllerDestino.cor = .systemGreen
        controllerDestino.textoInformativo.text = "ABRIU NO MODO\nFULLSCREEN"
        navigationViewController.modalPresentationStyle = .fullScreen

        present(navigationViewController, animated: true, completion: nil)
        
    }
    
    
    
    func addSubview(){
        view.addSubview(botaoModal)
        view.addSubview(botaoFullscreen)
        view.addSubview(navButton.btnVoltar)
    }
    
    
    func setupConstraints(){
        
        botaoModal.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 100).isActive = true
        botaoModal.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoModal.heightAnchor.constraint(equalToConstant: 40).isActive = true
        botaoModal.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        botaoFullscreen.bottomAnchor.constraint(equalTo: botaoModal.bottomAnchor, constant: 50).isActive = true
        botaoFullscreen.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoFullscreen.heightAnchor.constraint(equalToConstant: 40).isActive = true
        botaoFullscreen.widthAnchor.constraint(equalToConstant: 300).isActive = true
    }

}



// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::



class DismissViewController: UIViewController {
    
    var textoInformativo: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Arial Bold", size: 20)
        label.text = "PÁGINA ABERTA"
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let botaoFechar: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("FECHAR", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(fecharViewController), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var cor: UIColor = .yellow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = cor

        addSubview()
        setupConstraints()
    }

    func addSubview(){
        self.view.addSubview(textoInformativo)
        self.view.addSubview(botaoFechar)
    }
    
    func setupConstraints(){
        
        textoInformativo.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        textoInformativo.centerYAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerYAnchor).isActive = true
        
        botaoFechar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -100).isActive = true
        botaoFechar.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoFechar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoFechar.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
    
    @objc func fecharViewController(){
        self.dismiss(animated: true, completion: nil)
        
//        let controllerDestino = PresentViewController()
//        navigationController?.pushViewController(controllerDestino, animated: true)
    }


}
