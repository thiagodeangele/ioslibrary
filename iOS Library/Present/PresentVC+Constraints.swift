//
//  ViewController+Constraints.swift
//  Teste Visual
//
//  Created by Thiago de Angele on 23/08/21.
//

import Foundation

extension PresentVC {
    
    func configConstraints() {
    
        navButton.btnVoltar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -30).isActive = true
        navButton.btnVoltar.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        navButton.btnVoltar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        navButton.btnVoltar.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
    
}
