//
//  ViewController.swift
//  Swift Library
//
//  Created by Thiago de Angele on 18/08/21.
//

import UIKit

class ViewController: UIViewController {

    let botaoLink: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Link", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 18)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(acaoBotao), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let botaoDataPicker: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("UIDatePickerView", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 18)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(acaoDataPicker), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    let botaoScroll: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("UIScrollView", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 18)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(acaoScroll), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let botaoTimer: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Timer", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 18)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(acaoTimer), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let botao = ModelButton(titulo: "Abrir", altura: 50, largura: 200, tamanhoFonte: 25, principal: true, borda: true).criaBotao()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(botaoLink)
        view.addSubview(botaoDataPicker)
        view.addSubview(botaoScroll)
        view.addSubview(botaoTimer)
        view.addSubview(botao)
        
        setupConstraints()
        
        botao.addTarget(self, action: #selector(acaoBotaoNovo), for: .touchUpInside)
    }
    
    
    @objc func acaoBotao(){
        let paginaDestino = PresentVC()
        paginaDestino.modalPresentationStyle = .fullScreen
        present(paginaDestino, animated: true, completion: nil)
    }
    
    
    @objc func acaoDataPicker(){
        let paginaDestino = DatePickerVC()
        paginaDestino.modalPresentationStyle = .fullScreen
        present(paginaDestino, animated: true, completion: nil)
    }
    
    @objc func acaoScroll(){
        let paginaDestino = ScrollVC()
        paginaDestino.modalPresentationStyle = .fullScreen
        present(paginaDestino, animated: true, completion: nil)
    }
    
    @objc func acaoTimer(){
        let paginaDestino = TimerVC()
        paginaDestino.modalPresentationStyle = .fullScreen
        present(paginaDestino, animated: true, completion: nil)
    }
    
    
    
    @objc func acaoBotaoNovo(){
        print("Ok")
    }
    

    func setupConstraints(){
        
        botaoLink.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 100).isActive = true
        botaoLink.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoLink.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoLink.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        botaoDataPicker.bottomAnchor.constraint(equalTo: botaoLink.bottomAnchor, constant: 80).isActive = true
        botaoDataPicker.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoDataPicker.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoDataPicker.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        botaoScroll.bottomAnchor.constraint(equalTo: botaoDataPicker.bottomAnchor, constant: 80).isActive = true
        botaoScroll.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoScroll.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoScroll.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        botaoTimer.bottomAnchor.constraint(equalTo: botaoScroll.bottomAnchor, constant: 80).isActive = true
        botaoTimer.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoTimer.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoTimer.widthAnchor.constraint(equalToConstant: 300).isActive = true

        botao.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -30).isActive = true
        botao.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
    }
    
    
    
}

