//
//  TimerExemplo.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

class TimerExemplo: UIViewController {
    
    var segundos = 0
    var timer = Timer()


    
    func botaoHint() {
        timer.invalidate()
        segundos = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        if segundos < 5 {
            segundos += 1
            // Mostra o hint
        } else {
            timer.invalidate()
            segundos = 0
            // Esconde o hint e reinicia a contagem
            
        }
    }
    
}
