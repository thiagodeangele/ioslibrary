//
//  Timer+Funcoes.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

extension TimerVC {
    
    
    @objc func botaoFechar(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func iniciaContagem() {
        timer.invalidate()
        objects.btnIniciarContagem.isHidden = true
        botaoVoltar.btnVoltar.isHidden = true
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateTimer() {
        if segundos < seguntosTotais {
            segundos += 1
            objects.labelSegundos.text = "\(segundos)"
            objects.progressBar.isHidden = false
            objects.progressBar.progress = Float(segundos) / Float(seguntosTotais)
        } else {
            timer.invalidate() // Para o timer
            segundos = 0
            objects.labelSegundos.text = "FIM"
            botaoVoltar.btnVoltar.isHidden = false
            objects.btnIniciarContagem.isHidden = false
            objects.progressBar.isHidden = true
        }
    }
    
    
    
    
    
}
