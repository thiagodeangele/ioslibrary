//
//  TimerObjects.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

class TimerObjects: UIView {

    var labelTitulo: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Arial Bold", size: 25)
        label.text = "CONTAGEM REGRESSIVA"
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var labelSegundos: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Arial Bold", size: 70)
        label.text = "0"
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let btnIniciarContagem: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("INICIAR", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = #colorLiteral(red: 0.5205744505, green: 0.8490985036, blue: 0.9690579772, alpha: 1)
        button.layer.cornerRadius = 25
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let progressBar: UIProgressView = {
        let bar = UIProgressView(progressViewStyle: .default)
        bar.setProgress(0.0, animated: true)
        bar.trackTintColor = UIColor.lightGray
        bar.tintColor = #colorLiteral(red: 1, green: 0.2980421782, blue: 0.6694016457, alpha: 1)
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    
}
