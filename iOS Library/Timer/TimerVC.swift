//
//  TimerVC.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

class TimerVC: UIViewController {
        
    let objects = TimerObjects()
    let botaoVoltar = BotaoVoltar()

    var segundos = 0
    var seguntosTotais = 5
    
    var timer = Timer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.7926428318, green: 1, blue: 0.8136397004, alpha: 1)

        objects.progressBar.isHidden = true
        
        view.addSubview(objects.labelTitulo)
        view.addSubview(objects.labelSegundos)
        view.addSubview(objects.btnIniciarContagem)
        view.addSubview(objects.progressBar)
        view.addSubview(botaoVoltar.btnVoltar)
        
        botaoVoltar.btnVoltar.addTarget(self, action: #selector(botaoFechar), for: .touchUpInside)
        objects.btnIniciarContagem.addTarget(self, action: #selector(iniciaContagem), for: .touchUpInside)
        
        
        addConstraints()
    }
    
    
    
}
