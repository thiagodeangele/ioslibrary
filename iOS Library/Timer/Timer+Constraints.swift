//
//  Timer+Constraints.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import Foundation

extension TimerVC {
    
    func addConstraints(){
        
        objects.labelTitulo.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -70).isActive = true
        objects.labelTitulo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        objects.labelSegundos.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        objects.labelSegundos.topAnchor.constraint(equalTo: objects.labelTitulo.bottomAnchor, constant: 10).isActive = true
        
        objects.btnIniciarContagem.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        objects.btnIniciarContagem.topAnchor.constraint(equalTo: objects.labelSegundos.bottomAnchor, constant: 20).isActive = true
        objects.btnIniciarContagem.heightAnchor.constraint(equalToConstant: 50).isActive = true
        objects.btnIniciarContagem.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        objects.progressBar.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        objects.progressBar.topAnchor.constraint(equalTo: objects.labelSegundos.bottomAnchor, constant: 20).isActive = true
        objects.progressBar.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.7).isActive = true
        
        botaoVoltar.btnVoltar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -30).isActive = true
        botaoVoltar.btnVoltar.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoVoltar.btnVoltar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoVoltar.btnVoltar.widthAnchor.constraint(equalToConstant: 300).isActive = true
    }
    
}
