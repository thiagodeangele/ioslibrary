//
//  ScrollVC.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

class ScrollVC: UIViewController {
   
    let objects = ScrollObjects()
    let botaoVoltar = BotaoVoltar()

    var view1Hight: CGFloat!
    var view2Hight: CGFloat!
    var view3Hight: CGFloat!
    var view4Hight: CGFloat!
    var view5Hight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(objects.scrollview)
        objects.scrollview.addSubview(objects.containerView)
        
        objects.containerView.addSubview(objects.view1)
        objects.containerView.addSubview(objects.view2)
        objects.containerView.addSubview(objects.view3)
        objects.containerView.addSubview(objects.view4)
        objects.containerView.addSubview(objects.view5)
        
        view.addSubview(botaoVoltar.btnVoltar)
        
        botaoVoltar.btnVoltar.addTarget(self, action: #selector(botaoFechar), for: .touchUpInside)
        
        configConstraints()
        configScroll()

    }
    
}
