//
//  Scroll+Constraints.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

extension ScrollVC {
    
    func configConstraints() {
        
        view1Hight = 300
        objects.view1.heightAnchor.constraint(equalToConstant: view1Hight).isActive = true
        objects.view1.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true

        view2Hight = 300
        objects.view2.heightAnchor.constraint(equalToConstant: view2Hight).isActive = true
        objects.view2.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        objects.view2.topAnchor.constraint(equalTo: objects.view1.bottomAnchor).isActive = true
        
        view3Hight = 300
        objects.view3.heightAnchor.constraint(equalToConstant: view3Hight).isActive = true
        objects.view3.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        objects.view3.topAnchor.constraint(equalTo: objects.view2.bottomAnchor).isActive = true
        
        view4Hight = 300
        objects.view4.heightAnchor.constraint(equalToConstant: view4Hight).isActive = true
        objects.view4.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        objects.view4.topAnchor.constraint(equalTo: objects.view3.bottomAnchor).isActive = true
        
        view5Hight = 300
        objects.view5.heightAnchor.constraint(equalToConstant: view5Hight).isActive = true
        objects.view5.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        objects.view5.topAnchor.constraint(equalTo: objects.view4.bottomAnchor).isActive = true

        
        botaoVoltar.btnVoltar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -30).isActive = true
        botaoVoltar.btnVoltar.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoVoltar.btnVoltar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoVoltar.btnVoltar.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
    }

}
