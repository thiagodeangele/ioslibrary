//
//  Modelo+ScrollConfig.swift
//  Swift Library
//
//  Created by Thiago de Angele on 24/08/21.
//

import Foundation

extension DatePickerVC {
    
    func configScroll(){
    
        objects.scrollview.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        objects.scrollview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        objects.scrollview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        objects.scrollview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        objects.containerView.topAnchor.constraint(equalTo: objects.scrollview.topAnchor).isActive = true
        objects.containerView.bottomAnchor.constraint(equalTo: objects.scrollview.bottomAnchor).isActive = true
        objects.containerView.trailingAnchor.constraint(equalTo: objects.scrollview.trailingAnchor).isActive = true
        objects.containerView.leadingAnchor.constraint(equalTo: objects.scrollview.leadingAnchor).isActive = true
        objects.containerView.widthAnchor.constraint(equalTo: objects.scrollview.widthAnchor).isActive = true
        
        //let hightContainer: CGFloat = 1000
         
        objects.containerView.heightAnchor.constraint(equalToConstant: 1000).isActive = true
        
    }
    
}
