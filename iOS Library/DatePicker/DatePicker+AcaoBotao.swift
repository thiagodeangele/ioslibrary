//
//  ModeloVC+AcaoBotao.swift
//  Swift Library
//
//  Created by Thiago de Angele on 24/08/21.
//

import UIKit

extension DatePickerVC {
    
    @objc func botaoFechar(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func dataSelecionada(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let selectedDate: String = dateFormatter.string(from: sender.date)
    }


}
