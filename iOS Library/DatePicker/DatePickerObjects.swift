//
//  ModeloObjects.swift
//  Swift Library
//
//  Created by Thiago de Angele on 24/08/21.
//

import UIKit

class DatePickerObjects : UIView {
    
    let scrollview: UIScrollView = { 
        let scroll = UIScrollView()
        scroll.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let datePickerView: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.preferredDatePickerStyle = .wheels
        let local = Locale(identifier: "pt-br")
        datePicker.locale = local

        // Configurando para exibir apenas o último ano
        let dataAtual: Date = Date()
        var calendario: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendario.timeZone = TimeZone(identifier: "UTC")!
        
        var componentes: DateComponents = DateComponents()
        componentes.calendar = calendario
        componentes.year = 0
        let maxData: Date = calendario.date(byAdding: componentes, to: dataAtual)!
        componentes.year = -1
        let minData: Date = calendario.date(byAdding: componentes, to: dataAtual)!
        
        datePicker.minimumDate = minData
        datePicker.maximumDate = maxData
        return datePicker
    }()
    
}
