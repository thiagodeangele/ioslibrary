//
//  ModeloVC.swift
//  Swift Library
//
//  Created by Thiago de Angele on 24/08/21.
//

import UIKit

class DatePickerVC: UIViewController {
    
    let navButton = BotaoVoltar()
    let objects = DatePickerObjects()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(objects.scrollview)
        objects.scrollview.addSubview(objects.containerView)
        objects.containerView.addSubview(objects.datePickerView)
        objects.containerView.addSubview(navButton.btnVoltar)
        
        configConstraints() //A diciona constraints aos objetos
        configScroll() // Configura Scroll

        navButton.btnVoltar.addTarget(self, action: #selector(botaoFechar), for: .touchUpInside)
        objects.datePickerView.addTarget(self, action: #selector(dataSelecionada(_:)), for: .valueChanged)

    }

}
