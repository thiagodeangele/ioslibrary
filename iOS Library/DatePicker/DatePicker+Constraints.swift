//
//  Modelo+Constraints.swift
//  Swift Library
//
//  Created by Thiago de Angele on 24/08/21.
//


import Foundation

extension DatePickerVC {
    
    func configConstraints() {
        
        objects.datePickerView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        objects.datePickerView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
                
        navButton.btnVoltar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -30).isActive = true
        navButton.btnVoltar.centerXAnchor.constraint(equalTo: self.view.layoutMarginsGuide.centerXAnchor).isActive = true
        navButton.btnVoltar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        navButton.btnVoltar.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
    }
    
}
