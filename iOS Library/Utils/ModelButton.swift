//
//  Button.swift
//  iOS Library
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

class ModelButton {
    
    let titulo: String
    let altura: CGFloat
    let largura: CGFloat
    let tamanhoFonte: CGFloat
    let borda: Bool
    let principal: Bool
    
    init(titulo: String, altura: CGFloat, largura: CGFloat, tamanhoFonte: CGFloat, principal: Bool, borda: Bool) {
        self.altura = altura
        self.largura = largura
        self.titulo = titulo
        self.tamanhoFonte = tamanhoFonte
        self.principal = principal
        self.borda = borda
    }
    
    func criaBotao() -> UIButton{
        
        if principal {
            let btnVoltar: UIButton = {
                let button = UIButton(type: .system)
                button.setTitle(titulo, for: .normal)
                button.titleLabel?.font = UIFont(name: "Arial Bold", size: tamanhoFonte)
                button.contentHorizontalAlignment = .center
                button.backgroundColor = UIColor.init(displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
                button.layer.cornerRadius = altura/2
                button.translatesAutoresizingMaskIntoConstraints = false
                return button
            }()
            
            if borda {
                btnVoltar.layer.cornerRadius = altura/2
            } else {
                btnVoltar.layer.cornerRadius = 5
            }
            
            btnVoltar.heightAnchor.constraint(equalToConstant: altura).isActive = true
            btnVoltar.widthAnchor.constraint(equalToConstant: largura).isActive = true
            
            return btnVoltar

            
        } else {
            let btnAbrir: UIButton = {
                let button = UIButton(type: .system)
                button.setTitle(titulo, for: .normal)
                button.titleLabel?.font = UIFont(name: "Arial Bold", size: tamanhoFonte)
                button.contentHorizontalAlignment = .center
                button.backgroundColor = UIColor.init(displayP3Red: 223/255, green: 213/255, blue: 214/255, alpha: 0.8)
                button.translatesAutoresizingMaskIntoConstraints = false
                return button
            }()
            
            if borda {
                btnAbrir.layer.cornerRadius = altura/2
            } else {
                btnAbrir.layer.cornerRadius = 5
            }
            
            btnAbrir.heightAnchor.constraint(equalToConstant: altura).isActive = true
            btnAbrir.widthAnchor.constraint(equalToConstant: largura).isActive = true
            
            return btnAbrir
        }
    }

}
