//
//  NavegacaoView.swift
//  Swift Library
//
//  Created by Thiago de Angele on 23/08/21.
//

import UIKit


class NavegacaoView: UIView {
    
    
    let botaoVoltar: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("LINK VIEWCONTROLLER", for: .normal)
        button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
        button.contentHorizontalAlignment = .center
        button.backgroundColor = .systemGray3
        button.addTarget(self, action: #selector(acaoVoltar), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc func acaoVoltar(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupConstraints(){
        
        botaoVoltar.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: -50).isActive = true
        botaoVoltar.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor).isActive = true
        botaoVoltar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        botaoVoltar.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
    }
    
    
}
