//
//  Teste.swift
//  iOS Library
//
//  Created by Thiago de Angele on 25/08/21.
//

import UIKit

class Teste {

    func botaoFechar(titulo: String, altura: CGFloat, largura: CGFloat, tamanhoFonte: CGFloat = 15) -> UIButton {
        
        let btnVoltar: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle(titulo, for: .normal)
            button.titleLabel?.font = UIFont(name: "Arial Bold", size: tamanhoFonte)
            button.contentHorizontalAlignment = .center
            button.backgroundColor = UIColor.init(displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            button.layer.cornerRadius = 25
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        
        btnVoltar.heightAnchor.constraint(equalToConstant: altura).isActive = true
        btnVoltar.widthAnchor.constraint(equalToConstant: largura).isActive = true
        
        return btnVoltar
        
    }
    
    func botaoAbrir(titulo: String, altura: CGFloat, largura: CGFloat, tamanhoFonte: CGFloat = 15) -> UIButton {
        
        let btnVoltar: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle(titulo, for: .normal)
            button.titleLabel?.font = UIFont(name: "Arial Bold", size: tamanhoFonte)
            button.contentHorizontalAlignment = .center
            button.backgroundColor = UIColor.init(displayP3Red: 223/255, green: 213/255, blue: 214/255, alpha: 0.8)
            button.layer.cornerRadius = 7
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        
        btnVoltar.heightAnchor.constraint(equalToConstant: altura).isActive = true
        btnVoltar.widthAnchor.constraint(equalToConstant: largura).isActive = true
        
        return btnVoltar
        
    }
    
    
}
