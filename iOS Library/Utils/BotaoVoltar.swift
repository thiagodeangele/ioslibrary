//
//  BotoesNavegacaoViewController.swift
//  Swift Library
//
//  Created by Thiago de Angele on 19/08/21.


import UIKit

class BotaoVoltar: UIView {

        let btnVoltar: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle("VOLTAR", for: .normal)
            button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
            button.contentHorizontalAlignment = .center
            button.backgroundColor = .systemGray3
            button.layer.cornerRadius = 25
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
    
}
